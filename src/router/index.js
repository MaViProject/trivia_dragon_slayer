import Vue from 'vue'
import VueRouter from 'vue-router'
import Character from '@/components/Character.vue'
import Battlefield from '@/components/Battlefield.vue'
import About from '@/components/About.vue'
//import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Character',
    component: Character
  },
  {
    path: '/Battlefield',
    name: 'Battlefield',
    component: Battlefield
  },
  {
    path: '/About',
    name: 'About',
    component: About
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
