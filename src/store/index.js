import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

//export default new Vuex.Store({
export const store = new Vuex.Store({
  state: {
    hero: {
      name: null,
      image: null,
      life: null,
      attack: null,
      defense: null,
      heal: null
    }
  },
  getters: {
    getHero: state => {
      return state.hero;
    }
  },
  mutations: {
    setHero: (state, char) => {
      state.hero = char;
    }
  },
  actions: {
    setHero: ({commit}, char) => {
      commit('setHero', char);
    }
  },
  modules: {
  }
})
